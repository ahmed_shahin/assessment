<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\DataProviderX;
use App\Models\DataProviderY;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private static $appVersion = "v1";

    private static $providerResponseStructure = [
        'id',
        'parentAmount',
        'Currency',
        'parentEmail',
        'statusCode',
        'status',
        'balance',
        'email'
    ];

    public function testAllUsers()
    {
        $response = $this->get('/');

        $response = $this->get('/api/'.self::$appVersion.'/users/');
        $data = $response->getData();
        $response->assertStatus(200);
        return $data;
    }

    public function testFilterUsers()
    {
        $response = $this->get('/');

        $response = $this->get('/api/'.self::$appVersion.'/users/'.'?provider=DataProviderX&statusCode=authorised&currency=AED');
        $data = $response->getData();
        $response->assertStatus(200);
        return $data;
    }
}
