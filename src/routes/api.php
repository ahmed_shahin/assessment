<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API', 'prefix' => 'v1'], function () {
    Route::post('uploadDataX', [App\Http\Controllers\API\HomeController::class, 'uploadDataX']);
    Route::post('uploadDataY', [App\Http\Controllers\API\HomeController::class, 'uploadDataY']);
    Route::get('users', [App\Http\Controllers\API\HomeController::class, 'users']);
});
