<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataProviderY extends Model
{
    use HasFactory;

    protected $table = "data_provider_y_s";

    protected $casts = ['id' => 'string'];

    protected $fillable = ['id', 'balance', 'currency', 'email', 'status', 'statusCode','created_at'];

    protected $dates = ['created_at'];
}
