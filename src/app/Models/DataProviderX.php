<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataProviderX extends Model
{
    use HasFactory;

    protected $table = "data_provider_x_e_s";

    protected $fillable = ['id', 'parentAmount', 'Currency', 'parentEmail', 'statusCode', 'status', 'registerationDate','parentIdentification'];
}
