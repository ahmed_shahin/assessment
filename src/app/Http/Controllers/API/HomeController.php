<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use App\Models\DataProviderX;
use App\Models\DataProviderY;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function uploadDataX() // Upload Providers X File and Read it
    {
        $file = request('file');
        $content = File::get($file);
        $content = json_decode($content);
        foreach ($content->users as $key=>$value) {
            //dd($value);
            $dataProviderX = new DataProviderX;
            $dataProviderX->parentAmount  = $value->parentAmount;
            $dataProviderX->Currency = $value->Currency;
            $dataProviderX->parentEmail = $value->parentEmail;
            $dataProviderX->statusCode = $value->statusCode;
            $dataProviderX->registerationDate = $value->registerationDate;
            $dataProviderX->parentIdentification = $value->parentIdentification;
            $dataProviderX->status = $this->getStatusX($value->statusCode);

            $dataProviderX->save();
        }

        return response()->json([
      'status' => true,
      'message' => 'dataProviderX data imported successfully'
      ], 200);
    }

    public function uploadDataY()  // Upload Providers Y File and Read it
    {
        $file = request('file');
        $content = File::get($file);
        $content = json_decode($content);
        foreach ($content->users as $key=>$value) {
            $dataProviderY = new DataProviderY;
            $dataProviderY->id  = $value->id;
            $dataProviderY->balance = $value->balance;
            $dataProviderY->currency = $value->currency;
            $dataProviderY->email = $value->email;
            $dataProviderY->status = $value->status;
            $dataProviderY->statusCode = $this->getStatusY($value->status);
            $dataProviderY->created_at = Carbon::createFromFormat('d/m/Y', $value->created_at)->format('Y-m-d');
            $dataProviderY->save();
        }

        return response()->json([
      'status' => true,
      'message' => 'dataProviderY data imported successfully'
      ], 200);
    }

    public function getStatusY($value) // return provider Y status Code
    {
        if ($value == 100) {
            $statusCode = 'authorised';
        }

        if ($value == 200) {
            $statusCode = 'decline';
        }

        if ($value == 300) {
            $statusCode = 'refunded';
        }

        return $statusCode;
    }

    public function getStatusX($value) // return provider X status
    {
        if ($value == 1) {
            $status = 'authorised';
        }

        if ($value == 2) {
            $status = 'decline';
        }

        if ($value == 3) {
            $status = 'refunded';
        }
        return $status;
    }

    public function users(Request $request)
    {
        $dataX = DataProviderX::all();
        $dataY = DataProviderY::all();

        $data = $dataX->merge($dataY);

        if (request('provider') == 'DataProviderX') {
            $data = DataProviderX::all();
        }

        if (request('provider') == 'DataProviderY') {
            $data = DataProviderY::all();
        }

        if (request('statusCode')) {
            switch (request('statusCode')) {
            case 'authorised':
              // code...
              //dd($data);
              $dataX = $data->where('status', 'authorised');
              $dataY = $data->where('statusCode', 'authorised');

              $data = $dataX->merge($dataY);
              break;

              case 'decline':
                // code...
                $dataX = $data->where('status', 'decline');
                $dataY = $data->where('statusCode', 'decline');

                $data = $dataX->merge($dataY);

                break;

                case 'refunded':
                  // code...
                  $dataX = $data->where('status', 'refunded');
                  $dataY = $data->where('statusCode', 'refunded');

                  $data = $dataX->merge($dataY);

                  break;

            default:
              // code...
              break;
          }
        }


        if (request('balanceMin') && request('balanceMax')) {
            $dataX = $data->whereBetween('parentAmount', [request('balanceMin'), request('balanceMax')]);
            $dataY = $data->whereBetween('balance', [request('balanceMin'), request('balanceMax')]);

            $data = $dataX->merge($dataY);
        }

        if (request('currency')) {
            $dataX = $data->where('Currency', request('currency'));
            $dataY = $data->where('currency', request('currency'));

            $data = $dataX->merge($dataY);
        }

        return response()->json(['status' => true, 'data' => $data ], 200);
    }
}
