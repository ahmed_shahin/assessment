<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataProviderXESTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_provider_x_e_s', function (Blueprint $table) {
            $table->id();
            $table->float('parentAmount');
            $table->string('Currency');
            $table->string('parentEmail');
            $table->integer('statusCode');
            $table->enum('status', ['authorised','decline','refunded']);
            $table->date('registerationDate');
            $table->string('parentIdentification');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_provider_x_e_s');
    }
}
