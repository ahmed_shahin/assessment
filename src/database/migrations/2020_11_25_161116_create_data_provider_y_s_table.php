<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataProviderYSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_provider_y_s', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->float('balance');
            $table->string('currency');
            $table->string('email');
            $table->integer('status');
            $table->enum('statusCode', ['authorised','decline','refunded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_provider_y_s');
    }
}
